public class Persona
{
    private String nombres;
    private String apellidos;
    private String noIdentificacion;
    private String noCelular;
    private int edad;
    private int altura;
    private int peso;

    //Get y Set Para Nombres
    public String getNombre() {
        return nombres;
    }
    public void setNombre(String nombres) {
        this.nombres = nombres;
    }


    public String getApellidos(){return apellidos;}
    public void setApellidos(String apellidos){this.apellidos = apellidos;}

    //Get y Set Para noIdentificacion
    public String getNoIdentificacion() {return noIdentificacion;}
    public void setNoIdentificacion(String noIdentificacion) {
        this.noIdentificacion = noIdentificacion;
    }

    //Get y Set Para noCelular
    public String getNoCelular(){return noCelular;}
    public void setNoCelular(String noCelular){this.noCelular= noCelular;}

    //Get y Set Para Edad
    public int getEdad() {
        return edad;
    }
    public void setEdad(int edad) {
        this.edad = edad;
    }

    //Get y Set Para Altura
    public int getAltura(){return altura;}
    public void setAltura(int altura){this.altura=altura;}

    //Get y Set Para Peso
    public int getPeso(){return peso;}
    public void setPeso(int peso){this.peso=peso;}
}




