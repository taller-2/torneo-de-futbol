public class Jugador extends Persona
{
   private String posicion;
   private int dorsal;


    //Get y Set Para Posicion
    public String getPosicion() {return posicion;}
    public void setPosicion(String posicion) {this.posicion = posicion;}

    //Get y Set Para Dorsal
    public int getDorsal() {return dorsal;}
    public void setDorsal(int dorsal) {this.dorsal = dorsal;}
}
