import java.util.List;

public class Equipo
{
    private String nombreEquipo;
    private List<Jugador> listaJugadores;
    private Persona directorTecnico;
    private Persona preparadorFisico;
    private Persona kinesiologo;

    public Persona getKinesiologo() {return kinesiologo;}
    public void setKinesiologo(Persona kinesiologo) {this.kinesiologo = kinesiologo;}

    public List<Jugador> getListaJugadores() {
        return listaJugadores;
    }
    public void setListaJugadores(List<Jugador> listaJugadores) {
        this.listaJugadores = listaJugadores;
    }

    //Get y Set nombreEquipo
    public String getNombreEquipo() {
        return nombreEquipo;
    }
    public void setNombreEquipo(String nombreEquipo) {
        this.nombreEquipo = nombreEquipo;
    }

    //Get y Set directorTecnico
    public Persona getDirectorTecnico() {
        return directorTecnico;
    }
    public void setDirectorTecnico(Persona directorTecnico) {this.directorTecnico = directorTecnico;}

    //Get y Set preparadorFisico
    public Persona getPreparadorFisico() {return preparadorFisico;}
    public void setPreparadorFisico(Persona preparadorFisico) {this.preparadorFisico = preparadorFisico;}


}
