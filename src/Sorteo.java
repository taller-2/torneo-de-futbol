import org.w3c.dom.ls.LSOutput;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Sorteo {
    public static void main(String[] args)
    {
        String namesPlayers[] = {"Angel","Wilmar","Pablo","Edgar","Mario","Arturo", "Keylor", "Lionel", "Ronaldo", "James", "Radamel", "Rio", "Dani", "Thomas","Frank"};
        List<Equipo> listTeams = new ArrayList<>();
        int numbersTeams = 1;
        while (numbersTeams <= 4)
        {
            List<Jugador> listplayer = new ArrayList<>();
            for (int i = 0; i < 7; i++)
            {
                int rnd = new Random().nextInt(namesPlayers.length);
                Jugador jugador = new Jugador();
                jugador.setNombre(namesPlayers[rnd]);
                jugador.setDorsal(rnd);
                listplayer.add(jugador);
            }

            Equipo equipo = new Equipo();

            equipo.setNombreEquipo("Equipo " + numbersTeams);

            Persona tecnico = new Persona();
            tecnico.setNombre("Director Tecnico " + numbersTeams);
            equipo.setDirectorTecnico(tecnico);

            Persona preparador = new Persona();
            preparador.setNombre("Preparador Fisico " + numbersTeams);
            equipo.setPreparadorFisico(preparador);

            Persona kinesiologo = new Persona();
            kinesiologo.setNombre("Kinesiologo "+ numbersTeams);
            equipo.setKinesiologo(kinesiologo);

            equipo.setListaJugadores(listplayer);
            listTeams.add(equipo);
            numbersTeams++;
        }

        for (int i = 0; i < listTeams.size(); i++) {
            Equipo equipo = listTeams.get(i);
            System.out.print("Jugadores del equipo " + equipo.getNombreEquipo()+" - ");
            System.out.println("Director Tecnico: " + equipo.getDirectorTecnico().getNombre());
            System.out.println("Preparador Fisico: " + equipo.getPreparadorFisico().getNombre());
            System.out.println("Kinesiologo: " + equipo.getKinesiologo().getNombre());
                        for (int j = 0; j < equipo.getListaJugadores().size(); j++) {
                System.out.println(equipo.getListaJugadores().get(j).getNombre());
            }
            System.out.println();
        }
    }
}
